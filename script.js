//console.log("Hello world");

// ES6 is also known as ECMAScript 2015
// ECMAScript is the standard that is used to create implementations of the language, one which is JavaScript.
// where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.);

// New features to JavaScript
console.log("ES6 Updates");

// [SECTION] Exponent Operator
console.log("----------------");
console.log("=> Exponent Operator");

//using Math Object Methods

const firstNum = Math.pow(8, 3);
console.log(firstNum);

// Using the exponent operator
const secondNum = 8 ** 3;
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/

console.log("----------------");
console.log("=>Template Literals");

let name = "John";

// Pre-template Literals
let message = "Hello " + name + "! Welcome to programming";

console.log("Message without template literals");
console.log(message);

//Strings using template literals
//Uses backticks (``) instead of using (" ") or (' ')

message = `Hello ${name}! Welcome to programming!`;
//we use ${} to access a value or variable

console.log("Message with template literals");
console.log(message);


console.log("----------------");
console.log("=>Array Destructuring");
//[SECTION] Array Destructure
// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/
const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array Desctructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);
console.log("----------------");

// Array Destructuring
//variable naming for array destructuring is based on the developer's choice

const [fistName, middleName, lastName] = fullName;
console.log(fistName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${fistName} ${middleName} ${lastName}! It is nice to meet you!`);

console.log("----------------");
console.log("=>Object Destructuring");

// [SECTION] Object Destructuring
/*
	- Shortens the syntax for accessing properties form object
	- The difference with array destructuring, this should be exact property name.
	-Syntax:
		let/const {propertyNameA, propertyNameB, propertyNameC} = object;

*/

console.log("----------");
console.log("=> Object Destructuring");

const person = {
	givenName: "Jane",
	mName: "Dela",
	surName: "Cruz"
}

//Pre-object destructuring
console.log(person.givenName);
console.log(person.mName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.mName} ${person.SurName}! It's good to see you again`);

console.log("----------");

// Object Destructure
const {givenName, mName, surName} = person;
console.log(givenName);
console.log(mName);
console.log(surName);

console.log(`Hello ${givenName} ${mName} ${surName}! It's good to see you again`);


console.log("----------");
console.log("=>Arrow Functions");

//[SECTION] Arrow Functions
/*	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other position of code
	- This will work with "function expression". (s17)
	Example of funcExpression:

	let funcExpression = function funcName(){
		console.log("Hello from the other side")
	}
	funcExpression();
*/
/* Syntax:
		let/const variableName = (parameter) => {
			code to execute;
		}
		//invocation
		variableName(argument);
*/

const hello = () => {
	console.log("Hello from the other side");
}

hello();

//Pre-arrow function
const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student`);
})

// forEach method with the use of arrow function
console.log("----------");

/*
	Syntax:

		arrayName.arrayMethod((parameter) =>
			//code to execute;
			);
*/
students.forEach((student) =>
	console.log(`${student} is a student`));

// anonymous function - a function that has no function name

console.log("----------");
console.log("=>Implicit Return using Arrow Functions");
// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even withour the return keyword;
// const add = (x, y) =>{
// 	return x + y;
// }

/*
	Syntax:
		let/const variableName = (parameter/s) =>
		code to execute;
*/
const add = (x,y) => x+y;

let total = add(1,2);
console.log(total);

/*function add(x,y){
	return x+y;
}
let total = add(1,2);
console.log(total);*/

//[SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good morning, ${name}`;
console.log(greet());
console.log(greet("John"));

console.log("----------");
console.log("=>Class-Based Object Blueprint");

//[SECTION] Class-Based Object Blueprint
// Another approach in creating an object with key and value;
// Allows creation/instantiation of object using classes as blueprints.

// The "constructors" is a special method of a class for creating/initializing object for that class

/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// "new" operator creates/instantiates a new object with the given argument as value of it's property

const myCar = new Car();
console.log(myCar);

//Reassigning value of each property
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);